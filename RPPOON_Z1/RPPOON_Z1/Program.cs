﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("First Chapter", "Beggining of a story"));
            notebook.AddNote(new Note("Last Chapter", "End of a story"));
            notebook.AddNote(new Note("Author", "The name of author is unknown"));
            IAbstractIterator iterator = notebook.GetIterator();
            iterator.Current.Show();
            while(iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
