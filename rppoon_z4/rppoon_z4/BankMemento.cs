﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rppoon_z4
{
    class BankMemento
    {
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set; }

        public BankMemento(string name, string adress, decimal balance)
        {
            this.OwnerName = name; 
            this.OwnerAddress = adress; 
            this.Balance = balance;
        }
        public BankMemento(BankAccount account)
        {
            this.OwnerName = account.OwnerName; 
            this.OwnerAddress = account.OwnerAddress; 
            this.Balance = account.Balance;
        }

    }
}
