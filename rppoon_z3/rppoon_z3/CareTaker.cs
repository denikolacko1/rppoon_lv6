﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rppoon_z3
{
    class CareTaker
    {
        public List<Memento> previousStates { get; set; }
        public CareTaker()
        {
            this.previousStates = new List<Memento>();
        }
        public CareTaker(List<Memento> memento) 
        {
            this.previousStates = memento;
        }
        public void addState(Memento state)
        {
            previousStates.Add(state);
        }
        public Memento getMemento(int position)
        {
            return this.previousStates[position];
        }
    }
}
