﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rppoon_z3
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem toDo = new ToDoItem("Harry Potter", "First book", new DateTime(2003, 03, 16));
            Memento memento = toDo.StoreState();
            Console.WriteLine(toDo.ToString() );
            toDo.Rename("Game of Thrones");
            Memento secondMemento = toDo.StoreState();
            Console.WriteLine(toDo.ToString() );
            toDo.ChangeTask("Second Book");
            Console.WriteLine(toDo.ToString());




        }
    }
}
