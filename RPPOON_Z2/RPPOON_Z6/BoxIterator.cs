﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_Z2
{
    class BoxIterator : IAbstractIterator

    {
        private Box box;
        private int currentPosition;
        public BoxIterator(Box box)
        {
            this.box = box;
            this.currentPosition = 0;
        }
        public bool IsDone
        {
            get
            {
                return currentPosition >= box.Count;
            }
        }

        public Product Current
        {
            get
            {
                return box[this.currentPosition];
            }
        }

        public Product First()
        {
            return box[0];
        }

        public Product Next()
        {
            this.currentPosition++;
            if (this.IsDone)
            {
                return null;
            }
            else
            {
                return this.box[this.currentPosition];
            }
        }

    }
}
