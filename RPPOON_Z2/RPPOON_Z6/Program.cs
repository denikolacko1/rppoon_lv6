﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("Toys", 31));
            box.AddProduct(new Product("Books",100));
            box.AddProduct(new Product("Coca Cola",13.50));
            IAbstractIterator iterator = box.GetIterator();
            while (iterator.IsDone != true)
            {
                Console.WriteLine(iterator.Current);
                iterator.Next();    
            }

        }
    }
}
